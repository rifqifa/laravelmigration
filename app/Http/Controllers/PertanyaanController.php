<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('content.pertanyaan.create');
    }

    public function store(Request $request) {
        
        $request->validate([
            "title" => 'required',
            "body" => 'required'
        ]);
        $query = DB::table('questions')->insert([
            "judul" => $request["title"],
            "isi" => $request["body"]
            
        ]);

        return redirect('/pertanyaan')->with('success', 'Question has been posted');
    }

    public function index() {
        $pertanyaan = DB::table('questions')->get();
        return view('content.pertanyaan.index', compact('pertanyaan'));
    }

    public function show($pertanyaan_id) {
        $pertanyaan = DB::table('questions')->where('id', $pertanyaan_id)->first();
        return view('content.pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($pertanyaan_id) {
        $pertanyaan = DB::table('questions')->where('id', $pertanyaan_id)->first();
        return view('content.pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request) {
        $request->validate([
            "title" => 'required',
            "body" => 'required'
        ]);

        $query = DB::table('questions')
                ->where('id',$id)
                ->update([
                    'judul' => $request['title'],
                    'isi' => $request['body']
                ]);

        return redirect('/pertanyaan')->with('success', 'Question has been updated');
    }

    public function destroy($id) {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Question has been deleted');
    }
}
