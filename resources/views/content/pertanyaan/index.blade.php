@extends ('layouts.master')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Question Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success')}}
                </div>
              @endif
                <a class="btn btn-primary mb-2" href="pertanyaan/create">Ask New Question</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $tanya)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$tanya->judul}}</td>
                            <td>{{$tanya->isi}}</td>
                            <td style="display: flex;">
                                <a class="btn btn-info sm" href="pertanyaan/{{$tanya->id}}">Show</a>
                                <a class="btn btn-default sm" href="pertanyaan/{{$tanya->id}}/edit">Edit</a>
                                <form action="/pertanyaan/{{$tanya->id}}" method="post">
                                    @csrf 
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">No Questions</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


@endsection